#!/bin/bash -l
set -e
if [ "$#" -eq 0 ]; then
  exec hikrobotcamlib --help
else
  exec "$@"
fi

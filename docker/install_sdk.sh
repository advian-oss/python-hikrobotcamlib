#!/bin/bash
if [ -z "$SDK_URL" ]
then
  echo "\$SDK_URL must be set"
  exit 1
fi
set -ex
apt-get install -y curl unzip
mkdir /tmp/sdkinstall
cd /tmp/sdkinstall
curl "${SDK_URL:?must be defined}" -o sdk.zip
unzip sdk.zip
dpkg -i *$(uname -m)*.deb
cd /tmp
rm -rf /tmp/sdkinstall

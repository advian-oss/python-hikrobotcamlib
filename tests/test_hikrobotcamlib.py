"""Package level tests"""
from hikrobotcamlib import __version__

from hikrobotcamlib.errors import mv_errstr


def test_version() -> None:
    """Make sure version matches expected"""
    assert __version__ == "1.2.0"


def test_vendor() -> None:
    """Test the vendor import"""
    from hikrobotcamlib.hiklibs.MvCameraControl_class import MvCamera  # pylint: disable=C0415

    _cam = MvCamera()  # type: ignore[no-untyped-call]


def test_errstr() -> None:
    """Test the error string mapping"""
    assert mv_errstr(0) == "MV_OK"
    assert mv_errstr(0x80000007) == "MV_E_NODATA"

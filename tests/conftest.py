"""pytest automagics"""
import logging
import os

from libadvian.logging import init_logging


init_logging(logging.DEBUG)
LOGGER = logging.getLogger(__name__)


def real_hw_skipif() -> bool:
    """Return **True** if env is not correct"""
    env_int = int(os.environ.get("HIK_TEST_REAL", "0"))
    return not bool(env_int)

"""Test CLI scripts"""
import asyncio

import pytest
from libadvian.binpackers import ensure_str

from hikrobotcamlib import __version__
from .conftest import real_hw_skipif


@pytest.mark.asyncio
async def test_version_cli():  # type: ignore
    """Test the CLI parsing for default version dumping works"""
    cmd = "hikrobotcamlib --version"
    process = await asyncio.create_subprocess_shell(
        cmd,
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE,
    )
    out = await asyncio.wait_for(process.communicate(), 10)
    # Demand clean exit
    assert process.returncode == 0
    # Check output
    assert ensure_str(out[0]).strip().endswith(__version__)


@pytest.mark.skipif(real_hw_skipif(), reason="Define HIK_TEST_REAL if real hw is available")
@pytest.mark.asyncio
async def test_cli_output():  # type: ignore
    """Run the entrypoint and check output"""
    cmd = "hikrobotcamlib"
    process = await asyncio.create_subprocess_shell(
        cmd,
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE,
    )
    await asyncio.wait_for(process.communicate(), 10)
    # Demand clean exit
    assert process.returncode == 0

"""Test the camera wrapper"""
from typing import Generator, Optional, List
import asyncio
import logging

import pytest

from hikrobotcamlib.types import DeviceInfo, DeviceList
from hikrobotcamlib.wrapper import Camera
from hikrobotcamlib.types import Frame
from hikrobotcamlib.hiklibs.CameraParams_header import PixelType_Gvsp_BGR8_Packed, PixelType_Gvsp_RGB8_Packed

from .conftest import real_hw_skipif


LOGGER = logging.getLogger(__name__)
pytestmark = pytest.mark.skipif(real_hw_skipif(), reason="Define HIK_TEST_REAL if real hw is available")


# pylint: disable=W0621


@pytest.fixture
def first_device() -> Generator[DeviceInfo, None, None]:
    """Fixture yielding the first device in list"""
    for dev in DeviceList():
        yield dev
        break


def test_init_del(first_device: DeviceInfo) -> None:
    """Test the initializer and destructor"""
    cam = Camera(first_device)
    assert cam._mvcam  # pylint: disable=W0212
    assert cam.info.serialno == first_device.serialno
    LOGGER.debug("Got cam={}".format(cam))
    del cam


def test_open_close(first_device: DeviceInfo) -> None:
    """Test that we can open and close the camera"""
    cam = Camera(first_device)
    LOGGER.debug("Got cam={}".format(cam))
    assert cam.closed
    cam.open()
    assert not cam.closed
    cam.close()
    assert cam.closed


@pytest.mark.asyncio
async def test_frame_callback(first_device: DeviceInfo) -> None:
    """Test that we can handle callbacks"""
    cam = Camera(first_device)
    LOGGER.debug("Got cam={}".format(cam))
    got_frame: Optional[Frame] = None

    def frame_callback(frame: Frame, cam: Camera) -> None:
        """The callback the wrapper calls"""
        nonlocal got_frame
        got_frame = frame
        _ = cam

    cam.frame_callback = frame_callback
    cam.open()
    cam.set_exposure(1337)
    cam.trigger_enable(False)
    cam.start()

    async def frame_received() -> None:
        """Wait for frame and exit"""
        nonlocal got_frame
        while got_frame is None:
            await asyncio.sleep(0.25)

    await asyncio.wait_for(frame_received(), timeout=5.0)
    assert got_frame
    assert got_frame.data
    assert len(got_frame.data) == got_frame.len
    assert got_frame.exposure == 1337
    assert got_frame.type == cam.get_pixelformat()

    cam.stop()
    cam.close()


@pytest.mark.asyncio
async def test_sw_trigger(first_device: DeviceInfo) -> None:
    """Test that sw-trigger works"""
    cam = Camera(first_device)
    LOGGER.debug("Got cam={}".format(cam))
    frames: List[Frame] = []
    expect_frames = 5
    trigger_delay = 0.25

    def frame_callback(frame: Frame, cam: Camera) -> None:
        """The callback the wrapper calls"""
        nonlocal frames
        frames.append(frame)
        _ = cam

    cam.frame_callback = frame_callback
    cam.open()
    cam.set_trigger_source("Software")
    cam.trigger_enable(True)
    cam.set_bool("AcquisitionFrameRateEnable", False)
    cam.start()
    # Give it a moment
    await asyncio.sleep(0.25)
    # Send expected number of triggers
    for _ in range(expect_frames):
        cam.send_trigger()
        await asyncio.sleep(0.1)

    async def frame_received() -> None:
        """Wait for frame and exit"""
        nonlocal frames
        while len(frames) < expect_frames:
            await asyncio.sleep(trigger_delay)

    await asyncio.wait_for(frame_received(), timeout=expect_frames * trigger_delay + 1.0)
    assert len(frames) == expect_frames
    assert frames[0].frameno == 1
    assert frames[-1].frameno == expect_frames

    cam.set_bool("AcquisitionFrameRateEnable", True)
    cam.trigger_enable(False)
    cam.stop()
    cam.close()


@pytest.mark.asyncio
async def test_format_convert(first_device: DeviceInfo) -> None:
    """Test the convertsion"""
    cam = Camera(first_device)
    LOGGER.debug("Got cam={}".format(cam))
    got_frame: Optional[Frame] = None

    def frame_callback(frame: Frame, cam: Camera) -> None:
        """The callback the wrapper calls"""
        nonlocal got_frame
        got_frame = frame
        _ = cam

    cam.frame_callback = frame_callback
    cam.open()
    cam.set_exposure(5000)
    cam.trigger_enable(False)
    cam.start()

    async def frame_received() -> None:
        """Wait for frame and exit"""
        nonlocal got_frame
        while got_frame is None:
            await asyncio.sleep(0.25)

    await asyncio.wait_for(frame_received(), timeout=5.0)
    assert got_frame

    tgt_size = got_frame.size[0] * got_frame.size[1] * 3
    new_type, new_data = cam.pxformat_convert(got_frame, PixelType_Gvsp_RGB8_Packed, 3)
    assert new_type == "RGB8_Packed"
    assert len(new_data) == tgt_size

    new_type, new_data = cam.pxformat_convert(got_frame, PixelType_Gvsp_BGR8_Packed, 3)
    assert new_type == "BGR8_Packed"
    assert len(new_data) == tgt_size

    cam.stop()
    cam.close()

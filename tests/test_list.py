"""Test device listing"""
import logging

import pytest

from hikrobotcamlib.types import DeviceList, DeviceTransport, GigEDeviceInfo
from .conftest import real_hw_skipif


LOGGER = logging.getLogger(__name__)


@pytest.mark.skipif(real_hw_skipif(), reason="Define HIK_TEST_REAL if real hw is available")
def test_list_smoketest() -> None:
    """Test the list does not explode"""
    lst = DeviceList()
    assert lst._iteridx == 0  # pylint: disable=W0212


@pytest.mark.skipif(real_hw_skipif(), reason="Define HIK_TEST_REAL if real hw is available")
def test_list_realhw() -> None:
    """Test list with hw available"""
    lst = DeviceList()
    assert len(lst) > 0
    for dev in lst:
        assert dev
        assert dev.serialno
        assert dev.macaddr
        if dev.transport == DeviceTransport.GIGE:
            assert isinstance(dev.extra, GigEDeviceInfo)
            assert dev.extra.addr
        assert dev.model.manufacturer
        assert dev.model.model
        assert dev.model.version
